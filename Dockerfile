FROM centos:7
MAINTAINER uzzal, uzzal2k5@gmail.com
WORKDIR /admin
RUN yum install -y net-tools
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.25.0/install.sh | bash
RUN source ~/.bashrc
RUN yum install -y git
#RUN nvm ls-remote
#RUN nvm install v6.2.0
#RUN npm i -g npm
RUN git clone https://github.com/pol-is/polisClientAdmin.git
RUN cd /admin/polisClientAdmin
RUN echo "export SERVICE_URL=http://polis_server:5000" >.env_dev

ENTRYPOINT ["x"]


